# coding=utf-8
"""

3. 在OpenCV安装目录下找到课程对应演示图片(安装目录\sources\samples\data)，首先计算灰度直方图，进一步使用大津算法进行分割，并比较分析分割结果。
结果分析：
两幅图像的灰度直方图如图。
使用局部大津算法阈值分割，再使用形态学以及开运算去辅助处理图像，阈值分割效果明显，但是渐变色度的图像使用大津算法并没有很好的效果，是由于大津算法采用全局阈值。

"""

import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img1 = cv.imread(r'pic2.png')  # 读取图像
img2 = cv.imread(r'pic6.png')  # 读取图像
gray1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)  # 转换灰度图像
gray2 = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)  # 转换灰度图像
hist1 = cv.calcHist([gray1], [0], None, [256], [0, 256])  # 计算灰度直方图
hist2 = cv.calcHist([gray2], [0], None, [256], [0, 256])  # 计算灰度直方图

cv.imshow('pic2', img1)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/3_1.JPG
cv.imshow('pic6', img2)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/3_2.JPG

# 显示 hist1 灰度直方图
plt.plot(hist1)  # 绘制灰度直方图
plt.xlim([0, 256])  # 图像参数，绘制 X 轴的刻度
plt.title("Pic2__Histogram")  # 直方图标题
plt.show()  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/3_3.JPG

# 显示 hist2 灰度直方图
plt.plot(hist2)  # 绘制灰度直方图
plt.xlim([0, 256])  # 图像参数，绘制 X 轴的刻度
plt.title("Pic6_Histogram")  # 直方图标题
plt.show()  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/3_4.JPG

# pic2 大津算法阈值分割
Gauss_pic2 = cv.GaussianBlur(gray1, (5, 5), 0)  # 高斯滤波
_, gray_OTSU_pic2 = cv.threshold(Gauss_pic2, 125, 255, cv.THRESH_OTSU)  # 大津算法阈值分割
element_pic2 = cv.getStructuringElement(cv.MORPH_CROSS, (3,3))  # 数学形态学滤波
pic2 = cv.morphologyEx(gray_OTSU_pic2, cv.MORPH_OPEN, element_pic2)  # 开运算滤波
cv.imshow("Pic2_OTSU", pic2)  # 显示 pic2 经大津算法分割的图像 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/3_5.JPG

# pic6 大津算法阈值分割
Gauss_pic6 = cv.GaussianBlur(gray2, (5, 5), 0)  # 高斯滤波
_, gray_OTSU_pic6 = cv.threshold(Gauss_pic6, 125, 255, cv.THRESH_BINARY)  # 大津算法阈值分割
element_pic6 = cv.getStructuringElement(cv.MORPH_CROSS, (3,3))  # 数学形态学滤波
pic6 = cv.morphologyEx(gray_OTSU_pic6, cv.MORPH_OPEN, element_pic6)  # 开运算滤波
cv.imshow("Pic6_OTSU", pic6)  # 显示 pic6 经大津算法分割的图像 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/3_6.JPG

cv.waitKey()
cv.destroyAllWindows()
