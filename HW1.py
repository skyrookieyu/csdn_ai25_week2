# coding=utf-8
"""

1. 以Lena为原始图像，通过OpenCV实现平均滤波，高斯滤波及中值滤波，比较滤波结果。
结果分析：
1）对平均滤波、高斯滤波和中值滤波器均设置卷积核大小为 5*5，通过人眼分析运行结果图像，很明显，通过高斯滤波后，Lena 的睫毛和瞳孔依然比较清晰，而且帽子的纹路也仍然可以看清楚，反观其他两个就不是很清楚了，特别是平均滤波后的 Lena 整体呈现模糊的状态。通过察看 Lena 的帽子边缘可以看出，中值滤波后的边缘最为清晰。总体分析，高斯滤波效果最好，其次是中值滤波，最后是平均滤波。
2）对平均滤波、高斯滤波和中值滤波器设置不同卷积核大小时，例如，通过人眼分析运行结果图像，很明显，通过高斯滤波后，Lena 的睫毛和瞳孔依然比较清晰，而且帽子的纹路也仍然可以看清楚，反观其他两个就不是很清楚了，特别是平均滤波后的 Lena 整体呈现模糊的状态。通过察看 Lena 的帽子边缘可以看出，中值滤波后的边缘最为清晰。总体分析，高斯滤波效果最好，其次是中值滤波，最后是平均滤波。

"""

import cv2 as cv

LenaFilename = r'lena.jpg'

# 1）对平均滤波、高斯滤波和中值滤波器均设置卷积核大小为 5*5，通过人眼分析运行结果图像，很明显，通过高斯滤波后，Lena 的睫毛和瞳孔依然比较清晰，而且帽子的纹路也仍然可以看清楚，反观其他两个就不是很清楚了，特别是平均滤波后的 Lena 整体呈现模糊的状态。通过察看 Lena 的帽子边缘可以看出，中值滤波后的边缘最为清晰。总体分析，高斯滤波效果最好，其次是中值滤波，最后是平均滤波。
img = cv.imread(LenaFilename)
imgGauss = cv.GaussianBlur(img, (5, 5), 0)  # 高斯滤波: 注意的是a*b必须是奇数
imgblur = cv.blur(img, (5, 5))  # 均值滤波: 5*5的平均滤波器
medianBlur = cv.medianBlur(img, 5)  # 中值滤波: 第二个参数必须是基数且大于1，因为中值滤波就是将投射面排列取中间那个值

cv.imshow("Source", img)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/1_1.JPG
cv.imshow("Gaussian", imgGauss)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/1_2.JPG
cv.imshow("Blur", imgblur)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/1_3.JPG
cv.imshow("MedianBlur", medianBlur)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/1_4.JPG
cv.waitKey()
cv.destroyAllWindows()#显示图像

# 2）对平均滤波、高斯滤波和中值滤波器设置不同卷积核大小时，例如下面的示例，通过人眼分析运行结果图像，很明显，通过中值滤波后，Lena 的睫毛和瞳孔依然比较清晰，而且帽子的纹路也仍然可以看清楚，反观其他两个就不是很清楚了，特别是平均滤波后的 Lena 整体呈现模糊的状态。通过察看 Lena 的帽子边缘可以看出，中值滤波后的边缘最为清晰。总体分析，中值滤波效果最好，其次是高斯滤波，最后是平均滤波。
imgGauss2 = cv.GaussianBlur(img, (5, 5), 0)  # 高斯滤波: 注意的是a*b必须是奇数
imgblur2 = cv.blur(img, (1, 15))  # 均值滤波: 1*15的平均滤波器
medianBlur2 = cv.medianBlur(img, 3)  # 中值滤波: 第二个参数必须是基数且大于1，因为中值滤波就是将投射面排列取中间那个值

cv.imshow("Source", img)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/1_5.JPG
cv.imshow("Gaussian", imgGauss2)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/1_6.JPG
cv.imshow("Blur", imgblur2)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/1_7.JPG
cv.imshow("MedianBlur", medianBlur2)  # 运行结果:  https://gitee.com/skyrookieyu/csdn_ai25_week2/blob/master/1_8.JPG
cv.waitKey()
cv.destroyAllWindows()  # 显示图像

# 3) 综合分析：不同的卷积核大小会影响滤波的效果，实际运用的时候还是应该遵循目的和图片本身的特性去选择合适的算法与卷积核尺寸。
#             高斯平滑在图像平滑处理中表现较好，也是非常常用的一种滤波方式。中值滤波有利于椒盐噪声的处理，还有一定的保边作用。而平均滤波表现出的效果就感觉没有那么好。